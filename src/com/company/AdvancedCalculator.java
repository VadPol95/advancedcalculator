package com.company;

import java.io.*;
import java.nio.file.Path;
import java.util.Scanner;

/**
 * назначить символы что будут представлять MR M+ M- и сообщить об этом пользователю
 * число должно быть доступно так же после выключения(следующего включения) калькулятора
 * <p>
 * <p>
 * MR (Memory Read) - кнопка означает считать число из ячейки памяти и вывести его на дисплей.
 * M+ - прибавить к числу из памяти число, отображенное на дисплее и результат записать в память вместо предыдущего.
 * M- -
 * <p>
 * Для того,что бы вывести на экран последнее записанное число нужно ввести команду "R".
 * Для того,что бы использовать операцию М+ нужно провести какую либо из операций "+,-,*,/", после чего ввести символ "P"
 * и вывести на экран результат с помощью "R", такая же схема и М- с символом "M".
 */

public class AdvancedCalculator {
    private static double memory = 0;
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        start();
    }

    private static void start() {
        while (true)
            mathOperations(getOperation());
    }

    public static char getOperation() {
        System.out.println("+ add, - subtract, * multiply, / divide, R memoryRead, P memoryPlus, M - memoryMinus ");
        System.out.println("Enter operation: ");
        char operation;
        if (scanner.hasNext()) {
            operation = scanner.next().charAt(0);
        } else {
            System.out.println("Character input error, try again ");
            scanner.next();
            operation = getOperation();
        }
        return operation;
    }

    public static double getDouble() {
        System.out.println("Enter number:  ");
        double number;
        if (scanner.hasNextDouble()) {
            number = scanner.nextDouble();
        } else {
            System.out.println("Number input error, try again ");
            scanner.next();
            number = getDouble();
        }
        return number;
    }

    public static void mathOperations(char operation) {
        switch (operation) {
            case '+' -> add(getDouble(), getDouble());
            case '-' -> subtract(getDouble(), getDouble());
            case '*' -> multiply(getDouble(), getDouble());
            case '/' -> divide(getDouble(), getDouble());
            case 'R' -> memoryRead();
            case 'P' -> memoryPlus(memory);
            case 'M' -> memoryMinus(memory);
        }

    }

    public static double add(double number1, double number2) {
        System.out.println(number1 + " + " + number2 + " = " + (number1 + number2));
        memory = number1 + number2;
        return memory;
    }

    public static double subtract(double number1, double number2) {
        System.out.println(number1 + " - " + number2 + " = " + (number1 - number2));
        memory = number1 - number2;
        return memory;
    }

    public static double multiply(double number1, double number2) {
        System.out.println(number1 + " * " + number2 + " = " + (number1 * number2));
        memory = number1 * number2;
        return memory;
    }

    public static double divide(double number1, double number2) {
        System.out.println(number1 + " / " + number2 + " = " + (number1 / number2));
        memory = number1 / number2;
        return memory;
    }

    public static void recordFile(double value) {
        File file = Path.of("memory.txt").toFile();
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(file);
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }
        pw.println(value);
        pw.close();

    }

    public static double readFile() {
        File file = Path.of("memory.txt").toFile();
        String input = "";
        Scanner sc = null;
        try {
            sc = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println(readFile());
        }
        input = sc.nextLine();
        sc.close();
        return Double.parseDouble(input);
    }

    public static double memoryPlus(double value) {
        double memoryNum = 0;
        memoryNum = readFile();
        double result = memoryNum + value;
        recordFile(result);
        return result;
    }

    public static double memoryMinus(double value) {
        double memoryNum = 0;
        memoryNum = readFile();
        double result = memoryNum - value;
        recordFile(result);
        return result;
    }

    public static void memoryRead() {
        System.out.println(readFile());
    }
}